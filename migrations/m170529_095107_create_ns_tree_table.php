<?php

use yii\db\Migration;

/**
 * Handles the creation of table `ns_tree`.
 */
class m170529_095107_create_ns_tree_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('ns_tree', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->null(),
            'left_key' => $this->integer()->null(),
            'right_key' => $this->integer()->null(),
            'level' => $this->integer()->null(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('ns_tree');
    }
}
