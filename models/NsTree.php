<?php

namespace app\models;

use Yii;
use PDO;

/**
 * This is the model class for table "ns_tree".
 *
 * @property integer $id
 * @property string $name
 * @property integer $left_key
 * @property integer $right_key
 * @property integer $level
 */
class NsTree extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'ns_tree';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['left_key', 'right_key', 'level'], 'required'],
            [['left_key', 'right_key', 'level'], 'integer'],
            [['name'], 'string', 'max' => 50]
        ];
    }

    public function clearTable() {
        $this->deleteAll();
        $command = Yii::$app->db->createCommand("ALTER TABLE `" . $this::tableName() . "` AUTO_INCREMENT = 1")->execute();
    }

    public function getMaxKey() {
        return NsTree::find()->max('right_key');
    }

    public function getRoot() {
        return $this->find()
                        ->where(['level' => 1])
                        ->orderBy('left_key')
                        ->all();
    }

    public function getChildren() {
        return $this->find()
                        ->where(['>=', 'left_key', $this->left_key])
                        ->andWhere(['<=', 'right_key', $this->right_key])
                        ->orderBy('left_key')
                        ->all();
    }

    public function getParent() {
        return $this->find()
                        ->where(['<=', 'left_key', $this->left_key])
                        ->andWhere(['>=', 'right_key', $this->right_key])
                        ->orderBy('left_key')
                        ->all();
    }

    public function insertNode() {
        //Если нулевой уровень - значит родителя нет
        if ($this->level == 0) {
            $this->right_key = $this->getMaxKey() + 1;
        }

        $command = Yii::$app->db->createCommand('UPDATE `' . $this::tableName() 
                . '` SET right_key = right_key + 2, left_key = IF(left_key > :right_key, left_key + 2, left_key) WHERE right_key >= :right_key');
        $query = $command->bindValue(':right_key', $this->right_key, PDO::PARAM_INT)->execute();

        $node = new NsTree();
        $node->left_key = $this->right_key;
        $node->right_key = $this->right_key + 1;
        $node->level = $this->level + 1;
        $node->save();

        return $node;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'left_key' => 'Lft',
            'right_key' => 'Rgt',
            'level' => 'Level',
        ];
    }

}
