<?php

namespace app\controllers;

use Yii;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\NsTree;
use app\models\TreeForm;

class ThreadController extends \yii\web\Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'generate' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex($id = "") {
        $model = new TreeForm();
        return $this->render('index', [
                    'paramsUrl' => $id,
                    'model' => $model,
        ]);
    }

    private function arrayChildren($root = null, $items = []) {
        $result = [];
        $children = [];
        $index = 0;

        foreach ($items as $item) {
            if (($item->left_key > $root->left_key) && ($item->right_key < $root->right_key) && ($item->level - 1 == $root->level)) {
                $element = $this->arrayChildren($item, $items);
                array_push($children, $element);
            }
        }
        //Проблемы с плагином на JS и с одинаковыми ID, для этого сделал ID уникальными
        $id = md5($root->id . uniqid());
        $result = ['id' => $id, 'level' => $root->level, 'state' => ['opened' => true], 'text' => $root->id, 'children' => $children];

        return $result;
    }

    public function actionShow($id = "") {
        //Экшен для получения JSON массива элементов
        Yii::$app->response->format = Response::FORMAT_JSON;
        $elements = [];
        $showZeroNode = false;

        if ($id == "") {
            $roots = (new NsTree())->getRoot();
        } else {
            $items = explode(',', $id);
            $roots = NsTree::find()->where(['in', 'id', $items])->all();

            //Показываем "Нулевой элемент или нет"
            $showZeroNode = count($items) > 1 && count($roots) > 1;
        }

        foreach ($roots as $root) {
            if ($root !== null) {
                $items = $root->getChildren();
                $children = $this->arrayChildren($root, $items);
                array_push($elements, $children);
            }
        }
        //Если заданы id но элементы не найдены, вызываем полное дерево
        if (($elements == []) && ($id !== "")) {
            return $this->actionShow();
        }

        $return = $showZeroNode ? ["id" => 0, "text" => "0", 'state' => ['opened' => true], "children" => $elements] : $elements;

        return $return;
    }

    public function actionGenerate($count = 20) {
        $model = new TreeForm();
        //Думаю тут объяснять нечего, логика понятна
        if (Yii::$app->request->post()) {
            $count = (int) Yii::$app->request->post('count');
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->generate();
            }
        }

        return $this->renderAjax('_form', [
                    'model' => $model,
        ]);
    }

}
