<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use app\assets\JsTreeAsset;

JsTreeAsset::register($this);

$url = Url::to(['/thread/show']) . (isset($paramsUrl) ? "/" . $paramsUrl : "");

//Обновляем дерево при загрузке страницы
$this->registerJs(
        "$('#jstree').jstree({
            'core': {
                'data': {
                    'url': '$url',
                    'dataType': 'json' // needed only if you do not supply JSON headers
                }
            }
        });"
);
//Обновляем дерево при генерации с формы
$this->registerJs(
        "$('#generate-form').on('pjax:end', function() {
            $('#jstree').jstree('refresh');  
        });"
);
?>

<h2>Nested sets generation</h2>
<div class="row">
    <div class="col-xs-4">
        <?php \yii\widgets\Pjax::begin(['id' => 'generate-form', 'enablePushState' => false]); ?>
        <?=
        $this->render('_form', [
            'model' => $model
        ]);
        ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
    <div class="col-xs-7">
        <div id="jstree"></div>
    </div>
</div>
