-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Ноя 30 2017 г., 19:42
-- Версия сервера: 10.1.28-MariaDB
-- Версия PHP: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `task1`
--

-- --------------------------------------------------------

--
-- Структура таблицы `ns_tree`
--

CREATE TABLE `ns_tree` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `left_key` int(11) DEFAULT NULL,
  `right_key` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `ns_tree`
--

INSERT INTO `ns_tree` (`id`, `name`, `left_key`, `right_key`, `level`) VALUES
(1, NULL, 1, 16, 1),
(2, NULL, 17, 22, 1),
(3, NULL, 23, 30, 1),
(4, NULL, 18, 19, 2),
(5, NULL, 2, 9, 2),
(6, NULL, 31, 36, 1),
(7, NULL, 3, 6, 3),
(8, NULL, 32, 33, 2),
(9, NULL, 24, 25, 2),
(10, NULL, 10, 15, 2),
(11, NULL, 4, 5, 4),
(12, NULL, 7, 8, 3),
(13, NULL, 26, 29, 2),
(14, NULL, 11, 14, 3),
(15, NULL, 34, 35, 2),
(16, NULL, 27, 28, 3),
(17, NULL, 12, 13, 4),
(18, NULL, 20, 21, 2),
(19, NULL, 37, 40, 1),
(20, NULL, 38, 39, 2);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `ns_tree`
--
ALTER TABLE `ns_tree`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `ns_tree`
--
ALTER TABLE `ns_tree`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
