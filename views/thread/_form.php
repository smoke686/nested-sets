<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<?php
$form = ActiveForm::begin([
            'action' => ['/thread/generate'],
            'options' => [
                'class' => '',
                'data-pjax' => true
            ],
            'id' => 'form_post',
            'method' => 'post'
        ]);
?>
<div class="form-group">
    <p>Please enter count node</p>
</div>
<?=
$form->field($model, 'count')->textInput([
    'placeholder' => 'Please enter count node',
    'value' => 20
])
?>
<div class="form-group">
    <?= Html::submitButton(Yii::t('app', 'Generate tree'), ['class' => 'btn btn-primary btn-block']) ?>
</div>
<?php ActiveForm::end(); ?>