<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;

use yii\base\Model;
use app\models\NsTree;

/**
 * Description of TreeForm
 *
 * @author Андрей
 */
class TreeForm extends Model {

    public $count;

    public function rules() {
        return [
            [['count'], 'required'],
            [['count'], 'number', 'min' => 1],
        ];
    }

    public function generate() {
        $tree = new NsTree();
        $tree->clearTable();
        for ($i = 0; $i < $this->count; $i++) {
            $numb = rand(0, $i);
            if ($numb == 0) {
                $tree->level = 0;
                $tree = $tree->insertNode();
            } else {
                $tree = NsTree::findOne($numb);
                if ($tree !== null) {
                    $tree = $tree->insertNode();
                }
            }
        }
    }

}
